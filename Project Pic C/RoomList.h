#include "Classes.h"
#include <vector>
#include <fstream>

class List; 
class Iterator;

class Node {
public:
	Node(std::string filename);
	int checkI(Item thing);//checks the room inventory for item. returns -1 if not there or position in vector if there 
	void Drop(Item junk); //put item in room
	void Rape(Item key); //take item from room
	bool dragon; //is there a dragon 
	//if there is a dragon, fight dragon first
	void assPuzzle(void *func);//assign function
	void change(); 
private:
	Node* previousN = NULL;
	Node* nextN = NULL;
	std::ifstream plot;
	int lineCount = 0; 
	void(*func)() = NULL; 
	std::vector<Item> stuff;
	std::vector<Monster> opponents;
	friend class Iterator; 
	friend class List;
};

class List {
public: 
	List(); 
	void push_back(std::string filename); 
	Iterator erase(Iterator iter); 
	Iterator begin(); 
	Iterator end(); 
private:
	Node* firstRoom; 
	Node* lastRoom; 
	friend class Iterator; 
};

class Iterator {
public: 
	Iterator(); 
	//std::string get() const; 
	void next(); 
	void previous(); 
	bool equals(Iterator b) const; 
private:
	Node* position; 
	List* container; 
	friend class List;
};

