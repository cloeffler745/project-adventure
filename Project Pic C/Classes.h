#pragma once
#include <string>
#include <sstream>
#include <vector>


class Weapon; 

class Item {
public: 
	Item(std::string n, std::string d);
	std::string getName(); //get item name
	std::string getDesc(); //get ddescription of item
private: 
	std::string name;
	std::string desc; 
};

class Character {
public: 
	Character(std::string n, int h, int a, Weapon* ar);
	void Keep(Item stuff); //Put item in inventory
	void Discard(Item stuff); //remove item from inventory
	void Have(); //list what is in inventory
	//attack function
private:
	Weapon* arm; 
	int health; 
	int armor; 
	//attack stats
	std::string name; 
	std::vector<Item> Inventory; 
};

class Monster {
public:
	Monster(std::string name); 
private: 
	std::string name; 
	int health; 
	int armor; 
};

class Weapon {
public:
	Weapon(std::string name, std::string type, int boost); //type = broad sword, dagger, magic staff Name = Only friend
	int attack(); 
private: 
	std::string name;
	std::string type; 
	int boost; 
};