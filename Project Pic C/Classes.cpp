#include "Classes.h"

/* ************************************************************
ITEM 
************************************************************** */

Item::Item(std::string n, std::string d) {
	name = n;
	desc = d;
}

std::string Item::getName() {
	return name; 
}

std::string Item::getDesc() {
	return desc;
}

/* ************************************************************
CHARACTER
************************************************************** */

Character::Character(std::string n, int h, int a, Weapon* ar) {
	name = n; 
	health = h;
	armor = a; 
	arm = ar;
}